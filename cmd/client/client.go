package client

import (
	"context"

	"git.begroup.team/platform-core/kitchen/l"
	"git.begroup.team/platform-transport/gogi6/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/balancer/roundrobin"
)

type Gogi6Client struct {
	pb.Gogi6Client
}

func NewGogi6Client(address string) *Gogi6Client {
	conn, err := grpc.DialContext(context.Background(), address,
		grpc.WithInsecure(),
		grpc.WithBalancerName(roundrobin.Name),
	)

	if err != nil {
		ll.Fatal("Failed to dial Gogi6 service", l.Error(err))
	}

	c := pb.NewGogi6Client(conn)

	return &Gogi6Client{c}
}
